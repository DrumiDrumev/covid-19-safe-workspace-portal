export enum deskStatus {
  available = 1,
  occupied = 2,
  forbidden = 3,
}
