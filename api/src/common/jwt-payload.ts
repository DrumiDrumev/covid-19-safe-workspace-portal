export class JWTPayload {
  id: number;
  displayName: string;
  username: string;
  role: string; 
}
