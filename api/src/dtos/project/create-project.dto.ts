export class CreateProjectDTO {
    id: number;
    title: string;
    userId: number;
}
