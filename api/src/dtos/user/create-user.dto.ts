import { IsNotEmpty, Length, IsString } from 'class-validator';
export class CreateUserDTO {
  @IsNotEmpty()
  @IsString()
  username: string;
  @IsNotEmpty()
  @Length(5, 8)
  @IsString()
  password: string;
  @IsNotEmpty()
  @IsString()
  displayName: string;
}
