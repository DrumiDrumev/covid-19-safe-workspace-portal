import { VacationResponseDTO } from './../vacation/return-Vacation.dto';
import { ProjectResponseDTO } from './../project/project-response.dto';

export class UserResponseDTO {
  id: number;
  username: string;
  project: ProjectResponseDTO;
  vacation: VacationResponseDTO;
  week: string;
}
