
export class CreateVacationDTO {
    userId: number;
    start: Date;
    end: Date;
    isDeleted: boolean;

}