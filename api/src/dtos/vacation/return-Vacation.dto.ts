export class VacationResponseDTO {
  start: Date;
  end: Date;
}
