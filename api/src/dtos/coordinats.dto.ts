import { WeekStatus } from 'src/common/weekStatus';
export class coordinatsDTO {
  x: number;
  y: number;
  userId: number;
  weekStatus: WeekStatus;
}
