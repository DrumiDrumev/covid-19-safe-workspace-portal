export class CreateWorkspaceDTO {
  width: number;
  height: number;
  countryId: number;
}
