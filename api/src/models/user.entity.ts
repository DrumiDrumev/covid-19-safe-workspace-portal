import { VacationResponseDTO } from './../dtos/vacation/return-Vacation.dto';
import { Vacation } from 'src/models/vacation.entity';
import { UserRole } from './../common/user-role';

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { Project } from './project.entity';
import { Workspace } from './workspace.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;
  @Column()
  displayName: string;

  @Column()
  password: string;
  @Column({ nullable: true })
  email: string;

  @Column({ type: 'enum', enum: UserRole, default: UserRole.Admin })
  role: UserRole;

  @Column('boolean', { default: false })
  isDeleted: boolean;

  @Column({
    nullable: true,
  })
  banEndDate: Date;

  @Column({ nullable: true })
  avatarUrl: string;

  @ManyToOne(
    () => Project,
    project => project.users,
  )
  project: Project;

  @OneToOne(
    () => Vacation,
    vacation => vacation.user,
  )
  vacation: VacationResponseDTO;

  @ManyToOne(
    () => Workspace,
    workspace => workspace.users,
  )
  workspace: Workspace;

  @Column({ nullable: true })
  week: string;
}
