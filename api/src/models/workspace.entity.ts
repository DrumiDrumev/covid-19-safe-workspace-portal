import { CovidData } from './covidData.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { User } from './user.entity';

@Entity('workspaces')
export class Workspace {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => CovidData)
  @JoinColumn()
  country: CovidData;

  @OneToMany(
    () => User,
    users => users.workspace,
  )
  users: User[];

  @Column({ nullable: true })
  currentWeek: string;
  @Column({ nullable: true })
  nextWeek: string;

  width: number;
  height: number;
}
