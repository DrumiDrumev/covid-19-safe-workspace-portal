import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity('vacations')
export class Vacation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  start: Date;

  @Column()
  end: Date;
  @OneToOne(() => User)
  @JoinColumn()
  user: User;
  @Column({ type: 'boolean', default: false })
  isDeleted: boolean;
}
