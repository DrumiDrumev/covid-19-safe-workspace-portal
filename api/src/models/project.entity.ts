import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { User } from './user.entity';

@Entity('projects')
export class Project {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  title: string;
  
  @OneToMany(
    () => User,
    user => user.project,
  )
  users: User[];
}
