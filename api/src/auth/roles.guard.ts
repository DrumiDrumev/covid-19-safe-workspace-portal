import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Request } from 'express';
import { UserRole } from '../common/user-role';
import { User } from '../models/user.entity';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(role: UserRole, secondRole?: UserRole, thirdRole?: UserRole) {
    this.role = role;
    this.secondRole = secondRole;
    this.thirdRole = thirdRole;
  }

  private role: UserRole;
  private secondRole: UserRole;
  private thirdRole: UserRole;

  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest<Request>();
    const user = request.user as User;

    return user?.role === this.role
      ? true
      : user?.role === this.secondRole
      ? true
      : user?.role === this.thirdRole;
  }
}
