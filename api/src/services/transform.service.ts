import { Vacation } from 'src/models/vacation.entity';
import { User } from 'src/models/user.entity';
import { UserResponseDTO } from 'src/dtos/user/user-response.dto';
import { VacationResponseDTO } from '../dtos/vacation/return-Vacation.dto';

export class TransformService {
  toUserResponseDTO(user: User): UserResponseDTO {
    return {
      id: user.id,
      username: user.username,
      project: user.project
        ? {
            id: user.project.id,
            title: user.project.title,
          }
        : null,
      vacation: user.vacation
        ? {
            start: user.vacation.start,
            end: user.vacation.end,
          }
        : null,
      week: user.week,
    };
  }
  toReturnVacationResponseDTO(vacation: Vacation): VacationResponseDTO {
    return {
      start: vacation.start,
      end: vacation.end,
    };
  }
}
