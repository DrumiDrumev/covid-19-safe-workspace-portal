import { JWTPayload } from './../../common/jwt-payload';
import { jwtConstants } from './../../constants/secret';
import { AuthService } from './../auth.service';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { Injectable } from '@nestjs/common';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  async validate(payload: JWTPayload) {
    const user = await this.authService.findUserByName(payload.username);
    if (!user) {
      return;
    }

    if (user.banEndDate && user.banEndDate.valueOf() > Date.now()) {
      return false;
    }

    return user;
  }
}
