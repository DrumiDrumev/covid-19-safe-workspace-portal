import { Vacation } from './../models/vacation.entity';
import { Token } from './../models/token.entity';
import { JwtStrategy } from './strategy/jwt-strategy';
import { jwtConstants } from './../constants/secret';
import { User } from './../models/user.entity';
import { UserService } from './user.service';
import { Module, HttpModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TransformService } from './transform.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { CovidData } from 'src/models/covidData.entity';
import { CovidDataService } from './covidData.service';
import { Workspace } from 'src/models/workspace.entity';
import { WorkspaceService } from './workspace.service';
import { VacationService } from './vacation.service';
import { ProjectService } from './project.service';
import { Project } from 'src/models/project.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mariadb',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'root',
      database: 'covidofficialdb',
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([
      User,
      Token,
      CovidData,
      Workspace,
      Vacation,
      Project,
    ]),
    PassportModule,
    HttpModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {
        expiresIn: '7d',
      },
    }),
  ],
  providers: [
    UserService,
    TransformService,
    AuthService,
    JwtStrategy,
    CovidDataService,
    WorkspaceService,
    VacationService,
    ProjectService,
  ],
  exports: [
    UserService,
    AuthService,
    CovidDataService,
    WorkspaceService,
    VacationService,
    ProjectService,
  ],
})
export class ServicesModule {}
