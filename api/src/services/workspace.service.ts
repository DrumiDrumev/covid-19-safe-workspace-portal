import { CovidData } from './../models/covidData.entity';
import { Workspace } from './../models/workspace.entity';
import { User } from './../models/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { BadRequestException, Injectable } from '@nestjs/common';
import { TransformService } from './transform.service';
import { coordinatsDTO } from 'src/dtos/coordinats.dto';
import { CreateWorkspaceDTO } from 'src/dtos/create-workspace.dto';

@Injectable()
export class WorkspaceService {
  constructor(
    @InjectRepository(Workspace)
    private workspaceRepository: Repository<Workspace>,

    @InjectRepository(CovidData)
    private covidDataRepository: Repository<CovidData>,

    @InjectRepository(User) private userRepository: Repository<User>,
    private readonly trasformer: TransformService,
  ) {}

  async createWorkspace(workspace: CreateWorkspaceDTO): Promise<Workspace> {
    const workspaceToCreate = this.workspaceRepository.create(workspace);
    const matrix = [];
    const rows = workspace.height;
    const cols = workspace.width;

    for (let row = 0; row < rows; row += 1) {
      matrix[row] = [];

      for (let col = 0; col < cols; col += 1) {
        matrix[row][col] = 1;
      }
    }
    const matrixToSend = JSON.stringify(matrix);

    const foundCountry = await this.covidDataRepository.findOne({
      where: {
        id: workspace.countryId,
      },
    });

    workspaceToCreate.country = foundCountry;
    workspaceToCreate.currentWeek = matrixToSend;
    workspaceToCreate.nextWeek = matrixToSend;

    return await this.workspaceRepository.save(workspaceToCreate);
  }

  async updateWorkspace(
    workspaceId: number,
    countryId: number,
  ): Promise<Workspace> {
    const wsToFound = await this.workspaceRepository.findOne({
      relations: ['country'],
      where: {
        id: workspaceId,
      },
    });

    const foundCountry = await this.covidDataRepository.findOne(countryId);
    wsToFound.country = foundCountry;

    return await this.workspaceRepository.save(wsToFound);
  }

  async createDesk(
    coordinats: coordinatsDTO,
    workspaceId: string,
  ): Promise<Workspace> {
    const foundWorkSpace = await this.workspaceRepository.findOne({
      where: {
        id: workspaceId,
      },
    });

    const foundUser = await this.userRepository.findOne({
      where: {
        id: coordinats.userId,
      },
    });

    const userId = this.trasformer.toUserResponseDTO(foundUser).id;
    const setMatrixOnCurrentWeek = JSON.parse(foundWorkSpace.currentWeek);
    const setMatrixOnNextWeek = JSON.parse(foundWorkSpace.nextWeek);

    for (const innerArray of setMatrixOnCurrentWeek) {
      if (
        innerArray.find(el => el === userId && userId !== 1 && userId !== 2)
      ) {
        throw new BadRequestException(
          `${foundUser.username} has already been asigned!`,
        );
      }
    }

    for (const innerArray of setMatrixOnNextWeek) {
      if (
        innerArray.find(el => el === userId && userId !== 1 && userId !== 2)
      ) {
        throw new BadRequestException(
          `${foundUser.username} has already been asigned!`,
        );
      }
    }

    if (coordinats.weekStatus === 'currentWeek') {
      setMatrixOnCurrentWeek[coordinats.x][coordinats.y] = foundUser.id;
    } else if (coordinats.weekStatus === 'nextWeek') {
      setMatrixOnNextWeek[coordinats.x][coordinats.y] = foundUser.id;
    }

    foundWorkSpace.currentWeek = JSON.stringify(setMatrixOnCurrentWeek);
    foundWorkSpace.nextWeek = JSON.stringify(setMatrixOnNextWeek);

    return await this.workspaceRepository.save(foundWorkSpace);
  }

  async getAll(): Promise<Workspace[]> {
    return await this.workspaceRepository.find({
      relations: ['country', 'users'],
    });
  }
  public async getById(id: number): Promise<Workspace> {
    return await this.workspaceRepository.findOne(id, {
      relations: ['country'],
    });
  }
}
