import { CovidData } from 'src/models/covidData.entity';
import { Injectable, HttpService } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class CovidDataService {
  constructor(
    @InjectRepository(CovidData)
    private covidDataRepository: Repository<CovidData>,
    private readonly http: HttpService,
  ) {}

  async createData(newData: CovidData): Promise<CovidData> {
    const data = this.covidDataRepository.create(newData);
    return await this.covidDataRepository.save(data);
  }

  async getAll(): Promise<CovidData[]> {
    return await this.covidDataRepository.find();
  }
}
