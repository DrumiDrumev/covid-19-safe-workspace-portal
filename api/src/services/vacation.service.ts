import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateVacationDTO } from 'src/dtos/vacation/create-vacation.dto';
import { User } from 'src/models/user.entity';
import { Vacation } from 'src/models/vacation.entity';
import { Repository } from 'typeorm';

@Injectable()
export class VacationService {
  constructor(
    @InjectRepository(Vacation)
    private readonly vacationRepository: Repository<Vacation>,
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {}

  async createVacation(vacation: CreateVacationDTO): Promise<Vacation> {
    const user = await this.userRepository.findOne({ id: vacation.userId });

    const vacationToSubmit = this.vacationRepository.create(vacation);
    vacationToSubmit.user = user;
    
    return await this.vacationRepository.save(vacationToSubmit);
  }

  async updateVacation(
    vacation: CreateVacationDTO,
    vacationId: number,
  ): Promise<Vacation> {
    const vacationToUpdate = await this.vacationRepository.findOne({
      id: vacationId,
    });

    vacationToUpdate.start = vacation.start;
    vacationToUpdate.end = vacation.end;

    return await this.vacationRepository.save(vacationToUpdate);
  }

  async deleteVacation(vacationId: number): Promise<Vacation> {
    const vacationToDelete = await this.vacationRepository.findOne({
      id: vacationId,
    });

    vacationToDelete.isDeleted = true;

    return await this.vacationRepository.save(vacationToDelete);
  }
}
