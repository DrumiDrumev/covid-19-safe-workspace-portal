import { CreateUserDTO } from '../dtos/user/create-user.dto';
import { TransformService } from './transform.service';
import { User } from './../models/user.entity';
import { Repository } from 'typeorm';
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserResponseDTO } from 'src/dtos/user/user-response.dto';
import * as bcrypt from 'bcrypt';
import { Project } from 'src/models/project.entity';

@Injectable()
export class UserService {
  constructor(
    private trasformer: TransformService,
    @InjectRepository(User) private userRepository: Repository<User>,
    @InjectRepository(Project) private projectRepository: Repository<Project>,
  ) {}
  async getAll(loggerUserId: number): Promise<UserResponseDTO[]> {
    const users = await this.userRepository.find({
      relations: ['project', 'vacation'],
    });
    return users.map(user => this.trasformer.toUserResponseDTO(user));
  }
  public async getById(id: number): Promise<UserResponseDTO> {
    const user = await this.userRepository.findOne(id, {
      relations: ['project', 'vacation'],
    });

    return user !== undefined
      ? this.trasformer.toUserResponseDTO(user)
      : undefined;
  }

  async createUser(newUser: CreateUserDTO): Promise<UserResponseDTO> {
    const userToFound = await this.userRepository.findOne({
      where: {
        username: newUser.username,
      },
    });

    if (userToFound) {
      throw new BadRequestException(`${userToFound.username} already exist!`);
    }

    const user = this.userRepository.create(newUser);
    user.password = await bcrypt.hash(user.password, 10);

    const createdUser = await this.userRepository.save(user);

    return this.trasformer.toUserResponseDTO(createdUser);
  }
  async banUser(userId: number, period: number) {
    const user = await this.userRepository.findOne(userId);
    user.banEndDate = new Date(Date.now() + period);

    return await this.userRepository.save(user);
  }

  async assignProject(userId: number, projectId: number): Promise<User> {
    const projectToАssign = await this.projectRepository.findOne({
      id: projectId,
    });

    const user = await this.userRepository.findOne({ id: userId });

    user.project = projectToАssign;
    return await this.userRepository.save(user);
  }
}
