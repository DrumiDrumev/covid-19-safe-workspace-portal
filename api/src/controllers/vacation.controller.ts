import { Body, Controller, Param, Patch, Post, Put } from '@nestjs/common';
import { CreateVacationDTO } from 'src/dtos/vacation/create-vacation.dto';
import { Vacation } from 'src/models/vacation.entity';
import { VacationService } from 'src/services/vacation.service';

@Controller('vacation')
export class VacationController {
  constructor(private readonly vacationService: VacationService) {}

  @Post()
  async createVacation(@Body() vacation: CreateVacationDTO): Promise<Vacation> {
    return await this.vacationService.createVacation(vacation);
  }
  @Patch(':id')
  async updateVacation(
    @Body() vacation: CreateVacationDTO,
    @Param('id') vacationId: string,
  ): Promise<Vacation> {
    return await this.vacationService.updateVacation(vacation, +vacationId);
  }
  @Put(':id')
  async deleteVacation(@Param('id') vacationId: string): Promise<Vacation> {
    return await this.vacationService.deleteVacation(+vacationId);
  }
}
