import { Body, Controller, Param, Post, Put } from '@nestjs/common';
import { Project } from 'src/models/project.entity';
import { ProjectService } from 'src/services/project.service';

@Controller('projects')
export class ProjectController {
    constructor(private readonly projectService: ProjectService) {}
    @Post()
    async createProject(@Body() project: Project): Promise<Project> {
      return await this.projectService.createProject(project);
    }

}
