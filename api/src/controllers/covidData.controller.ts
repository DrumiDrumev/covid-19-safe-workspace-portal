import { CovidData } from './../models/covidData.entity';
import { CovidDataService } from './../services/covidData.service';
import { Controller, Get, HttpService, Post } from '@nestjs/common';

@Controller('importData')
export class covidDataController {
  constructor(
    private readonly http: HttpService,
    private readonly covidDataService: CovidDataService,
  ) {}

  @Post()
  async root() {
    const response = await this.http
      .get('https://coronavirus-19-api.herokuapp.com/countries')
      .toPromise();
    const covidData = response.data;

    return await this.covidDataService.createData(covidData);
  }

  @Get()
  async all(): Promise<CovidData[]> {
    return this.covidDataService.getAll();
  }
}
