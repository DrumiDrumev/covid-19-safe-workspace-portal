import { LoginUserDTO } from './../dtos/user/login-user.dto';
import { AuthService } from './../services/auth.service';
import { Controller, Post, Body, Delete, Req } from '@nestjs/common';
import { Request } from 'express';

@Controller('session')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post()
  async login(@Body() userDto: LoginUserDTO): Promise<{ token: string }> {
    return await this.authService.login(userDto.username, userDto.password);
  }

  @Delete()
  async logout(@Req() request: Request): Promise<{ message: string }> {
    const token = request.headers.authorization;
    await this.authService.blacklist(token?.slice(7));

    return {
      message: 'You have been successfully logged out!',
    };
  }
}
