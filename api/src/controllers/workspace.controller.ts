import { Workspace } from './../models/workspace.entity';
import { WorkspaceService } from './../services/workspace.service';
import {
  Controller,
  Post,
  Body,
  Param,
  Put,
  Patch,
  Get,
  NotFoundException,
  UseGuards
} from '@nestjs/common';

import { coordinatsDTO } from 'src/dtos/coordinats.dto';
import { CreateWorkspaceDTO } from 'src/dtos/create-workspace.dto';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/common/user-role';

@Controller('workspaces')
export class WorkspaceController {
  constructor(private readonly workspaceService: WorkspaceService) {}

  @Post()
  async createMatrix(@Body() size: CreateWorkspaceDTO): Promise<Workspace> {
    return await this.workspaceService.createWorkspace(size);
  }

  @Put('/:workspaceId/country/:countryId')
  @UseGuards(
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic, UserRole.Banned),
  )
  async updateWorkspace(
    @Param('workspaceId') workspaceId: string,
    @Param('countryId') countryId: string,
  ): Promise<Workspace> {
    return await this.workspaceService.updateWorkspace(
      +workspaceId,
      +countryId,
    );
  }

  @Patch('/:workspaceId/users')
  @UseGuards(
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic, UserRole.Banned),
  )
  async createDesk(
    @Body() coordinats: coordinatsDTO,
    @Param('workspaceId') workspaceId: string,
  ): Promise<Workspace> {
    return await this.workspaceService.createDesk(coordinats, workspaceId);
  }

  @Get()
  @UseGuards(
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic, UserRole.Banned),
  )
  async all(): Promise<Workspace[]> {
    return this.workspaceService.getAll();
  }

  @Get(':id')
  @UseGuards(
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic, UserRole.Banned),
  )
  async getById(@Param('id') id: number): Promise<Workspace> {
    const workspace = await this.workspaceService.getById(id);

    if (workspace === undefined) {
      throw new NotFoundException(`No workspace with id: ${id}`);
    }

    return workspace;
  }
}
