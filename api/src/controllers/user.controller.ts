import { UserRole } from './../common/user-role';
import { RolesGuard } from './../auth/roles.guard';
import {
  Controller,
  Get,
  Post,
  Param,
  NotFoundException,
  Body,
  UseGuards,
  ValidationPipe,
  Put,
} from '@nestjs/common';
import { UserService } from './../services/user.service';
import { UserResponseDTO } from 'src/dtos/user/user-response.dto';
import { UserId } from 'src/auth/user-id.decorator';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { CreateUserDTO } from 'src/dtos/user/create-user.dto';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {} //Enjecting UserService to UserController by Injectable() decorator from user.service.ts

  @Get()
  @UseGuards(
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic, UserRole.Banned),
  )
  async all(@UserId() loggedUserId: number): Promise<UserResponseDTO[]> {
    return this.userService.getAll(loggedUserId);
  }

  @Get(':id')
  @UseGuards(
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic, UserRole.Banned),
  )
  async getById(@Param('id') id: number): Promise<UserResponseDTO> {
    const user = await this.userService.getById(id);

    if (user === undefined) {
      throw new NotFoundException(`No user with id: ${id}`);
    }

    return user;
  }

  @Post()
  async createUser(
    @Body(new ValidationPipe({ whitelist: true })) newUser: CreateUserDTO,
  ): Promise<UserResponseDTO> {
    const createdUser = await this.userService.createUser(newUser);
    return createdUser;
  }

  @Post(':id/ban')
  @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  async banUser(@Param('id') userId: string, @Body() banDto: any) {
    return await this.userService.banUser(+userId, banDto.period);
  }
  @Put('/:userId/projects/:projectId')
  @UseGuards(
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic, UserRole.Banned),
  )
  async assignProject(
    @Param('userId') user: string,
    @Param('projectId') project: string,
  ): Promise<UserResponseDTO> {
    return await this.userService.assignProject(+user, +project);
  }
}
