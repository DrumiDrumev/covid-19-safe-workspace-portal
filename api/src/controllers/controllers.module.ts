import { ServicesModule } from './../services/services.module';
import { Module, HttpModule } from '@nestjs/common';
import { UserController } from './user.controller';
import { AuthController } from './auth.controller';
import { covidDataController } from './covidData.controller';
import { WorkspaceController } from './workspace.controller';
import { VacationController } from './vacation.controller';
import { ProjectController } from './project.controller';

@Module({
  imports: [ServicesModule, HttpModule],
  controllers: [
    UserController,
    AuthController,
    covidDataController,
    WorkspaceController,
    VacationController,
    ProjectController,
  ],
})
export class ControllersModule {}
