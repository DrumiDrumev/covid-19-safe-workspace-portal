import React, { useContext } from "react";
import { NavLink, withRouter } from "react-router-dom";
import "./Navigation.css";
import UserContext from "../../providers/UserContext";
import ImportData from "../Pages/importData/ImportData";
function getRandomInt(bookId) {
  return Math.floor(Math.random() * Math.floor(bookId) + 1);
}

const randomBooksId = "/workspaces/" + getRandomInt(10).toString();

const Navigation = (props) => {
  const { user, setUser } = useContext(UserContext);
  const history = props.history;

  const logout = () => {
    setUser(null);
    localStorage.removeItem("token");
    history.push("/home");
  };

  return (
    <div className="navigation">
      <nav>
        <NavLink to="/" className="link">
          Home
        </NavLink>
        {user ? (
          <>
            <NavLink to="/workspaces/" className="link">
              Workspace
            </NavLink>
            <NavLink to="/users/" className="link">
              Users
            </NavLink>
            <NavLink to="/home" onClick={logout}>
              Logout
            </NavLink>
          </>
        ) : (
          <>
            <NavLink to="/login" className="link">
              Sign In
            </NavLink>
            <NavLink to="/register" className="link">
              Sign Up
            </NavLink>
          </>
        )}
      </nav>
    </div>
  );
};

export default withRouter(Navigation);
