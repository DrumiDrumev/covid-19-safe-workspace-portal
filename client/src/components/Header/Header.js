import React from "react";
import "./Header.css";

const Header = () => {
  return (
    <div className="Header">
      <h1>COVID-19 Safe Workspace Portal</h1>
    </div>
  );
};

export default Header;
