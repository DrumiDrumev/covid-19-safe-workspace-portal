import React from "react";
import "./Footer.css";

const Footer = () => {
  return (
    <div className="Footer">
      <footer>
        <h4>
          <div>
            <a href="dimo.nenkov.a20@learn.telerikacademy.com">
              dimo.nenkov.a20@learn.telerikacademy.com
            </a>
          </div>
          <div>
            <a href="drumi.drumev.a20@learn.telerikacademy.com">
              drumi.drumev.a20@learn.telerikacademy.com
            </a>
          </div>
        </h4>
      </footer>
    </div>
  );
};

export default Footer;
