import React, { useState, useEffect, createContext } from "react";
import { BASE_URL } from "../../common/constants";
import { withRouter } from "react-router-dom";
import dateFormat from "dateformat";
import "./Users.css";

const Users = ({ newWeek, whichWeek }) => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(false);
  const [state, setState] = useState(false);
  const [error, setError] = useState(null);
  const [deleteUserState, setDeleteUserState] = useState("");

  useEffect(() => {
    fetch(`${BASE_URL}/users`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token" || "")}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }

        const newUsers = [...result];
        setUsers(newUsers);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const findUser = (userId) => {
    if (newWeek) {
      for (const innerArray of newWeek) {
        if (innerArray.find((el) => el === userId)) {
          return true;
        }
      }
    }
  };

  const deleteUser = (userId) => {
    fetch(`${BASE_URL}/users/`, {
      method: "DELETE",
      headers: {
        //Authorization: `Bearer ${localStorage.getItem("token" || "")}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        const newUsers = users.filter((user) => user.id !== userId);
        setUsers(newUsers);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  };


  const findUserWeek = (userId) => {
    if (newWeek) {
      for (const innerArray of newWeek) {
        if (innerArray.find((el) => el === userId)) {
          return true;
        }
      }
    }
  };
  return (
    <div>
      <div>
        <div className="users-container">
          {users
            .filter((user) => user.id !== 1 && user.id !== 2)
            .map((user) =>
              user.id !== undefined && findUser(user.id) ? (
                <div
                  key={user.id}
                  style={{
                    textAlign: "center",
                    borderStyle: "solid",
                    margin: "5px",
                    padding: "10px",
                    backgroundColor: "pink",
                    width: "350px",
                  }}
                >
                  <div onClick={() => user.id}>
                    <h3>{user.username}</h3>

                    <div>
                      <div>
                        <h2>Vacation</h2>
                        <p>
                          FROM{" "}
                          {user.vacation
                            ? dateFormat(
                                `${user.vacation.start}`,
                                "mmmm dS yyyy"
                              )
                            : null}
                        </p>
                        <p>
                          TO{" "}
                          {user.vacation
                            ? dateFormat(`${user.vacation.end}`, "mmmm dS yyyy")
                            : null}
                        </p>
                      </div>
                    </div>
                  </div>
                  <div>
                    {" "}
                    {findUserWeek(user.id) ? (
                      <span style={{ color: "red" }}>OFFICE</span>
                    ) : (
                      <span style={{ color: "green" }}>ONLINE</span>
                    )}
                  </div>
                  <div>
                    PROJECT{" "}
                    {user.project ? (
                      <span style={{ color: "indigo" }}>
                        {user.project.title}
                      </span>
                    ) : (
                      <span style={{ color: "blue" }}> ONBOARDING</span>
                    )}
                  </div>
                </div>
              ) : (
                <div
                  onClick={() => deleteUser(user.id)}
                  key={user.id}
                  style={{
                    textAlign: "center",
                    borderStyle: "solid",
                    margin: "5px",
                    padding: "10px",
                    backgroundColor: "white",
                    width: "350px",
                  }}
                >
                  {" "}
                  <div>
                    <h3>{user.username}</h3>
                    <h2>Vacation</h2>
                    <p>
                      FROM{" "}
                      {user.vacation
                        ? dateFormat(`${user.vacation.start}`, "mmmm dS yyyy")
                        : null}
                    </p>
                    <p>
                      TO{" "}
                      {user.vacation
                        ? dateFormat(`${user.vacation.end}`, "mmmm dS yyyy")
                        : null}
                    </p>
                  </div>
                  <div>
                    {" "}
                    {findUserWeek(user.id) ? (
                      <span style={{ color: "red" }}>OFFICE</span>
                    ) : (
                      <span style={{ color: "green" }}>ONLINE</span>
                    )}
                  </div>
                  <div>
                    PROJECT{" "}
                    {user.project ? (
                      <span style={{ color: "indigo" }}>
                        {user.project.title}
                      </span>
                    ) : (
                      <span style={{ color: "blue" }}> ONBOARDING</span>
                    )}
                  </div>
                </div>
              )
            )}
        </div>
      </div>
    </div>
  );
};
export default withRouter(Users);
