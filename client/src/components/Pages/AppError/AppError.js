import React from "react";
//import PropTypes from 'prop-types';
import "./AppError.css";

const AppError = ({ message }) => {
  return (
    <div className="AppError">
      <h1>{message}</h1>
    </div>
  );
};

// AppError.propTypes = {
//   message: PropTypes.string.isRequired,
// };

export default AppError;
