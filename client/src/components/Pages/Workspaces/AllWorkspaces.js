import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { BASE_URL } from "../../../common/constants";
import "./SingleWorkspaces.css";

const AllWorkspaces = ({ history }) => {
  const [allWorkspaces, setAllWorkspaces] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/workspaces/`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token" || "")}`,
       "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }

        setAllWorkspaces(result);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const currentWeek = allWorkspaces.map((week) => JSON.parse(week.currentWeek));

  const [category, setCategory] = React.useState("");

  const handleCategoryChange = (category) => {
    setCategory(history.push(`/workspaces/${category}`));
  };

  return (
    <div>
      <div style={{ textAlign: "center" }}>
        {" "}
        Choose:
        <select
          name="category"
          value={category}
          onChange={(event) => handleCategoryChange(event.target.value)}
        >
          <option value="none" hidden>
            Country
          </option>
          <option value="1">USA</option>
          <option value="2">Germany</option>
          <option value="3">Bulgaria</option>
          <option value="4">Russia</option>
        </select>
      </div>
    </div>
  );
};
export default withRouter(AllWorkspaces);
