import React, { useState, useEffect, useContext } from "react";
import { withRouter } from "react-router-dom";
import { BASE_URL } from "../../../common/constants";
import "./SingleWorkspaces.css";
import ImportData from "../importData/ImportData";
import Users from "../../Users/Users";
import UserContext from "../../../providers/UserContext";

const SingleWorkspace = ({ history, match, week, key, users }) => {
  const [workspace, setWorkspace] = useState([]);
  const [desk, setDesk] = useState(false);
  const [loading, setLoading] = useState(false);
  const [state, setState] = useState(false);
  const [error, setError] = useState(null);
  const [userId, setUserId] = useState();
  const [space, setSpace] = useState(false);
  const [createSpace, setCreateSpace] = useState(false);
  const [number, setNumber] = useState(3);
  const [whichWeek, setWeekSchedule] = useState(true);

  const userContext = useContext(UserContext);
  const loggedUser = userContext.user || 1;

  const handleClick = () => {
    if (loggedUser.role === "Admin") {
      setNumber((prevState) =>
        prevState < 33 && !space && !createSpace ? prevState + 1 : prevState
      );
    } else {
      return alert("You are not admin!");
    }
  };

  const id = match.params["id"];
  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/workspaces/${id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token" || "")}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        if (result.workspace !== []) {
          setWorkspace(result);
        }
        setState(true);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false), setState(true));
  }, []);

  const currentWeek = workspace.currentWeek
    ? JSON.parse(workspace.currentWeek.slice())
    : [];
  const nextWeek = workspace.nextWeek
    ? JSON.parse(workspace.nextWeek.slice())
    : [];

  const clearDesk = () => {
    setSpace(!space);
    //setCreateSpace(!createSpace);
  };
  const createSpaceHandle = () => {
    setCreateSpace(!createSpace);
  };

  const changeWeek = () => {
    setWeekSchedule(!whichWeek);
  };

  const newWeek = whichWeek ? [...currentWeek] : [...nextWeek];

  const createDesk = (i, j) => {
    fetch(`${BASE_URL}/workspaces/${id}/users`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token" || "")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        x: i,
        y: j,
        userId: space ? 1 : createSpace ? 2 : number,
        weekStatus: whichWeek ? "currentWeek" : "nextWeek",
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }

        setDesk(true);
        setWorkspace(result);
      })

      .catch((error) => setError(error.message))
      .finally(() => setDesk(false), setLoading(false));
  };

  return (
    <div>
      <ImportData workspace={workspace} space={space} />
      <div> </div>
      <div
        className="container"
        style={{ textAlign: "center", fontSize: "25px" }}
      >
        {newWeek.map((row, i) => (
          <div key={i}>
            {row.map((col, j) =>
              col === 2 ? (
                <div
                  key={j}
                  onClick={() => {
                    loggedUser.role === "Admin" && createDesk(i, j);

                    handleClick();
                  }}
                  className="cellStyle"
                  style={{ backgroundColor: "green" }}
                >
                  {/* {i + "|" + j} */}
                  Available
                </div>
              ) : col >= 3 ? (
                <div
                  onClick={() => {
                    createDesk(i, j);
                  }}
                  className="cellStyle"
                  key={j}
                  style={{
                    backgroundColor: "red",
                  }}
                >
                  Occupied
                </div>
              ) : (
                <div
                  onClick={() => {
                    createDesk(i, j);
                  }}
                  className="cellStyle"
                  key={j}
                  style={{ borderColor: "cadetblue", color: "white" }}
                >
                  Empty
                </div>
              )
            )}
          </div>
        ))}
        <div>
          <h2>{whichWeek ? "Current Week" : "Next Week"}</h2>
          {loggedUser.role === "Admin" ? (
            <div style={{ textAlign: "center" }}>
              <button onClick={changeWeek}>
                {whichWeek ? "Next Week" : "Current Week"}
              </button>{" "}
              <button onClick={clearDesk}>
                {space ? "Clear Space ON" : "Clear Space OFF"}
              </button>{" "}
              <button onClick={createSpaceHandle}>
                {createSpace ? "Available ON" : "Available OFF"}
              </button>
            </div>
          ) : null}
          <img
            style={{ marginTop: "50px" }}
            src="https://www.texvet.org/sites/default/files/styles/medium/public/12027643_10153651683148748_8679713668627528460_n%5B1%5D.png?itok=PZ_4wMuD"
            alt="workspace"
          ></img>
        </div>
      </div>

      <Users newWeek={newWeek} whichWeek={whichWeek} />
    </div>
  );
};
export default withRouter(SingleWorkspace);
