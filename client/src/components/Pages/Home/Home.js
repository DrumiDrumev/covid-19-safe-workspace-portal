import React, { useEffect, useState } from "react";
import "./Home.css";

const Home = ({ history, state }) => {
  const [category, setCategory] = useState("");
  const [reaload, setReload] = useState(false);

  return (
    <div className="grid">
      <div>
        <h2>Home</h2>
        <a href="/workspaces/1">
          <img
            className="home-img"
            src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fimages.clipartpanda.com%2Foffice-people-images-free-vector-the-cartoon-office-daoshi-figure-vector_093929_the_cartoon_office_daoshi_figure_vector.jpg&f=1&nofb=1"
            alt="office"
          ></img>
          <div class="text-block">
            <h4>USA</h4>
          </div>
        </a>

        <a href="/workspaces/2">
          <img
            className="home-img"
            src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fimages.clipartpanda.com%2Foffice-people-images-free-vector-the-cartoon-office-daoshi-figure-vector_093929_the_cartoon_office_daoshi_figure_vector.jpg&f=1&nofb=1"
            alt="office"
          ></img>
          <div class="text-block-second">
          <h4>Germany</h4>
        </div>
        </a>
        <a href="/workspaces/3">
          <img
            className="home-img"
            src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fimages.clipartpanda.com%2Foffice-people-images-free-vector-the-cartoon-office-daoshi-figure-vector_093929_the_cartoon_office_daoshi_figure_vector.jpg&f=1&nofb=1"
            alt="office"
          ></img>
          <div class="text-block-third">
          <h4>Bulgaria</h4>
        </div>
        </a>
        <a href="/workspaces/4">
        <img
          className="home-img"
          src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fimages.clipartpanda.com%2Foffice-people-images-free-vector-the-cartoon-office-daoshi-figure-vector_093929_the_cartoon_office_daoshi_figure_vector.jpg&f=1&nofb=1"
          alt="office"
        ></img>
        <div class="text-block-fourth">
        <h4>Russia</h4>
      </div>
      </a>
      </div>
    </div>
  );
};
export default Home;
