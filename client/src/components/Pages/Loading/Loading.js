import React from "react";
//import PropTypes from "prop-types";
import "./Loading.css";

const Loading = ({ children }) => {
  return <div className="lds-dual-ring">{children}</div>;
};

export default Loading;
