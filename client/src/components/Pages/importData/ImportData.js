import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { BASE_URL } from "../../../common/constants";

const ImportData = ({ workspace, history }) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [singleCountry, setSingleCountry] = [];

  useEffect(() => {
    fetch(`${BASE_URL}/importData/`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token" || "")}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setData(result);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const foundedCountry = data.filter(
    (c) => workspace.country !== undefined && workspace.country.id === c.id
  );

  return (
    <div style={{ textAlign: "center" }}>
      <div>
        {foundedCountry.map((d, key) => (
          <div
            key={key}
            className="container"
            style={{ backgroundColor: "orange" }}
          >
            <h3>Country: {d.country}</h3>

            <h3>
              TODAY CASES: <span style={{ color: "red" }}>{d.todayCases}</span>{" "}
              | ALL CASES:<span style={{ color: "red" }}> {d.cases}</span> |
              TESTS PER MILLION:
              <span style={{ color: "blue" }}> {d.testsPerOneMillion}</span> |
              TOTAL TESTS:
              <span style={{ color: "blue" }}> {d.totalTests}</span>
            </h3>
          </div>
        ))}
      </div>
    </div>
  );
};
export default withRouter(ImportData);
