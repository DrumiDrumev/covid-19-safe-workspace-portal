import React, { useContext, useState } from "react";
import "./SignIn.css";
import { BASE_URL } from "../../common/constants";
import jwtDecode from "jwt-decode";
import UserContext from "../../providers/UserContext";

const SignIn = (props) => {
  const history = props.history;
  const location = props.location;

  const { setUser } = useContext(UserContext);

  const [user, setUserObject] = useState({
    username: "",
    password: "",
  });

  const updateUser = (prop, value) => setUserObject({ ...user, [prop]: value });

  const isLogin = location.pathname.includes("login");

  const login = () => {
    if (!user.username) {
      return alert("Invalid username");
    }
    if (!user.password) {
      return alert("Invalid password");
    }

    fetch(`${BASE_URL}/session`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(user),
    })
      .then((r) => r.json())
      .then((result) => {
        if (result.error) {
          return alert(result.message);
        }
        try {
          const payload = jwtDecode(result.token);
          setUser(payload);
        } catch (e) {
          return alert(e.message);
        }
        localStorage.setItem("token", result.token);
        history.push("/home");
      })
      .catch(alert);
  };

  const register = () => {
    if (!user.username) {
      return alert("Invalid username");
    }
    if (!user.password) {
      return alert("Invalid password");
    }

    fetch(`${BASE_URL}/users`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        ...user,
        displayName: user.username,
      }),
    })
      .then((r) => r.json())
      .then((result) => {
        if (result.error) {
          return alert(result.message);
        }

        history.push("/login");
      })
      .catch(alert);
  };

  return (
    <div className="signIn-form">
      <>
        <h1>{isLogin ? "Login" : "Register"}</h1>
        <label htmlFor="input-username">Username</label>
        <input
          type="text"
          id="input-username"
          placeholder="username"
          value={user.username}
          onChange={(e) => updateUser("username", e.target.value)}
        />
        <label htmlFor="input-password">Password</label>
        <input
          type="password"
          id="input-username"
          value={user.password}
          onChange={(e) => updateUser("password", e.target.value)}
        />
        <br />
        <br />
        {isLogin ? (
          <button onClick={login}>Login</button>
        ) : (
          <button onClick={register}>Register</button>
        )}
      </>
    </div>
  );
};

export default SignIn;
