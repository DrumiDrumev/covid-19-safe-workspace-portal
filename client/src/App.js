import React, { useState } from "react";
import "./App.css";
import { BrowserRouter, Switch, Redirect, Route } from "react-router-dom";
import Navigation from "./components/Navigation/Navigation";
import Header from "./components/Header/Header";
import Home from "./components/Pages/Home/Home";
import SingleWorkspace from "./components/Pages/Workspaces/SingleWorkspace";
import AllWorkspaces from "./components/Pages/Workspaces/AllWorkspaces";
import ImportData from "./components/Pages/importData/ImportData";
import Users from "./components/Users/Users";
import UserContext, { getLoggedUser } from "./providers/UserContext";
import SignIn from "./components/SignIn/SignIn";

function App() {
  const [user, setUser] = useState(getLoggedUser);

  console.log(`User is logged: ${!!user}`);

  return (
    <div>
      <BrowserRouter>
        <UserContext.Provider value={{ user, setUser }}>
          <Header />

          <Navigation />
          <Switch>
            <Redirect path="/" exact to="/home" />
            <Route path="/home" component={Home} />
            <Route path="/workspaces" exact component={AllWorkspaces} />
            <Route path="/workspaces/:id" component={SingleWorkspace} />
            <Route path="/importData" component={ImportData} />
            <Route path="/users" component={Users} />
            <Route path="/login" component={SignIn} />
            <Route path="/register" component={SignIn} />
          </Switch>
        </UserContext.Provider>
      </BrowserRouter>
    </div>
  );
}

export default App;
